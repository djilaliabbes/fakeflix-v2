# fakeflix

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve

### Mail and PW for testing app
create compte 
https://www.themoviedb.org/

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

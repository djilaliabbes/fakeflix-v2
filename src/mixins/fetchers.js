export default{
  methods: {
    getData:function(url){
      return fetch(url)
            .then(response => {
              if(response.ok) return response.json();
            })
            .then(json => {return json})
            .catch(error => console.log(error));
          
   },
   postData:function(url,body){
    return fetch(url,{
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(body)
            })
            .then(response => {
              if(response.ok) return response.json()
            })
            .then(json => {
              console.log('json post data',json)
              return json
            })
            .catch(error => console.log(error));  
  },
  }

}

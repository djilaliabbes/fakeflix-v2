import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';
import fetchers from './mixins/fetchers'

Vue.config.productionTip = false


new Vue({
  el: '#app',
  mixins:[fetchers],
  data(){
    return {
      
    }
  },

  computed:{

  },

  created:async function(){
      this.getData(this.$store.state.urlDiscover)
      .then(res=>{
        if(res && res.results) this.$store.state.discover = res.results;
      })
      .catch(err => console.log(err))
  },
  methods:{
    logout: function () {
      this.$session.destroy();
      this.$store.commit('session_id',false);
      this.$router.push('/login')
    }
  },
  router,
  store,
  render: h => h(App),
}).$mount('#app')

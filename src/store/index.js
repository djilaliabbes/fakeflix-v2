import Vue from 'vue'
import Vuex from 'vuex'
import fetchers from '../mixins/fetchers';



Vue.use(Vuex)
const store =  new Vuex.Store({
    state:{
        urlMovie : `${process.env.VUE_APP_BASE_URL}search/movie?api_key=${process.env.VUE_APP_API_KEY}&query=`,
        baseImgUrl : "http://image.tmdb.org/t/p/w342",
        urlDiscover:`${process.env.VUE_APP_BASE_URL}discover/movie?api_key=${process.env.VUE_APP_API_KEY}&sort_by=popularity.desc&include_adult=false&include_video=false`,
        urlConnction : `${process.env.VUE_APP_BASE_URL}authentication/token/validate_with_login?api_key=${process.env.VUE_APP_API_KEY}`,
        urlToken: `${process.env.VUE_APP_BASE_URL}authentication/token/new?api_key=${process.env.VUE_APP_API_KEY}`,
        urlSession: `${process.env.VUE_APP_BASE_URL}authentication/session/new?api_key=${process.env.VUE_APP_API_KEY}`,
        session_id:'',
        isFavorite:false,
        inWatchList:false,
        query:'',
        discover:[],
        similarMovies:[],
        account:'',
        movies: [],
        favorites:[],
        watchList:[],
        movie:{},
        video:{},
        spinner:false,
        error:false,
    },
    mutations:{
      setMovies(state,res){
        state.movies = res;
      },
      setFavorites(state,res){
        state.favorites = res;
      },
      setDiscover(state,res){
        state.discover = res;
      },
      setWatchList(state,res){
        state.watchList = res;
      },
      setError(state,error){
        state.error = error;
      },
      setVideo(state,res){
          state.video = res;
      },
      setMovie(state,res){
        state.movie = res;
      },
      setSimilarMovies(state,res){
        state.similarMovies = res;
      },
      setSpinner(state,status){
        state.spinner = status;
      },
      setQuery(state,query){
        state.query = query;
      },
      setIsFavorite(state,bool){
        state.isFavorite = bool;
      },
      setInWatchList(state,bool){
        state.inWatchList = bool
      },
    },
    actions:{
        async setMovies({commit,state}){
           let json = await fetchers.methods.getData(state.urlMovie+state.query);
            if(json && json.results){
              commit('setMovies',json.results);
              commit('setSpinner',false);
            }
            
        },
        async setFavorites({commit,getters}){
          let res = await fetchers.methods.getData(getters.urlFavorites);
          if(res && res.results) commit('setFavorites',res.results);
        },
        async setMovie({commit,getters},id){
            let res = await fetchers.methods.getData(getters.getUrl(id));
            if(res) commit('setMovie',res);
        },
        async setSimilarMovies({commit,getters},id){
          console.log(getters.urlSimilarMovies(id))
          let res = await fetchers.methods.getData(getters.urlSimilarMovies(id));
          console.log("res similar movies",res)
          if(res) commit('setSimilarMovies',res.results);
        },
        async setVideo({commit,getters},id){
          let json = await fetchers.methods.getData(getters.getUrl(id,'/videos'));
          if(json.results) commit('setVideo',json.results[0]);
        },
        async checkIsFavoris({getters},id){
          console.log(id)
          let res = await fetchers.methods.getData(getters.urlFavOrWatch('favorite/movies'));
          if(res && res.results){
            return res.results.some(obj=>obj.id == id);
          }

        },
        async checkIsInWatchList({getters},id){
          console.group(id)
          let res = await fetchers.methods.getData(getters.urlFavOrWatch('watchlist/movies'))
          if(res && res.results){
            return res.results.some(obj=>obj.id == id);
          }
        },
        async setFavoriteOrWatchList({getters},...arg){
          console.log(arg);
          let arr = arg[0];
          
          let body = {
              media_type: "movie",
              media_id: arr[2],
          }
          body[arr[0]] = arr[1];
          console.log(body)
         return await fetchers.methods.postData(getters.urlFavOrWatch(arr[0]),body);

        }
    },
    getters: {
        urlAccount: () => {
          return `${process.env.VUE_APP_BASE_URL}account?api_key=${process.env.VUE_APP_API_KEY}&session_id=${sessionStorage.getItem('session_id')}`;
        },
        urlFavorites: ()=>{
            return `${process.env.VUE_APP_BASE_URL}account/${sessionStorage.getItem('account_id')}/favorite/movies?api_key=${process.env.VUE_APP_API_KEY}&session_id=${sessionStorage.getItem('session_id')}`;
        },
        urlWatchList: ()=>{
          return `${process.env.VUE_APP_BASE_URL}account/${sessionStorage.getItem('account_id')}/watchlist/movies?api_key=${process.env.VUE_APP_API_KEY}&session_id=${sessionStorage.getItem('session_id')}`
        },
        urlSimilarMovies:() =>movie_id =>{
              return `${process.env.VUE_APP_BASE_URL}movie/${movie_id}/similar?api_key=${process.env.VUE_APP_API_KEY}&language=en-US&page=1`
          
        },
        srcVideo:state=>{
          return `http://www.youtube.com/embed/${state.video.key}?autoplay=0&`;
        },
        releaseDate:state =>{
            let options = {weekday: "long", year: "numeric", month: "long", day: "numeric"}
            return  new Date(state.movie.release_date).toLocaleDateString('fr-FR',options);
        },
        getUrl:() =>(id,arg = '')=>{
            return `https://api.themoviedb.org/3/movie/${id}${arg}?api_key=${process.env.VUE_APP_API_KEY}`;
          
        },
        urlFavOrWatch:()=> name =>{
          return `${process.env.VUE_APP_BASE_URL}account/${sessionStorage.getItem('account_id')}/${name}?api_key=${process.env.VUE_APP_API_KEY}&session_id=${sessionStorage.getItem('session_id')}`
      },

      }
})
export default store;
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import WatchList from '../views/WatchList.vue'
import Favorites from '../views/Favorites.vue'
import MovieDetails from '../views/MovieDetails'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Carousel3d from 'vue-carousel-3d';
Vue.use(Carousel3d);
import VueSession from 'vue-session'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.use(VueSession)
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

const routes = [
    {
        path:'/',
        name:'Home',
        component :Home
    },
    {
        path:'/login',
        name:'Login',
        component :Login
    },
    {
        path:'/movie/:id',
        name:'MovieDetails',
        component :MovieDetails
    },
    {
        path:'/favorites',
        name:'Favorites',
        component :Favorites
    },
    {
        path:'/watchlist',
        name:'WatchList',
        component :WatchList
    }
]

const router = new VueRouter({
    mode : 'history',
    routes
})
export default router;
